parseConfig <- function(conf = CONF) {
  if ( ! any(conf$lang == c(1, 2))) {
    stop("Wrong language index!");
  }
}