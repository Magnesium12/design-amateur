num_to_char <- function(vn = 0) { # Creates a Excel-like numeration. By the way, thats a pain in the ass
  vsAlphabet <- c( # vector with alphabet
    " ", # The zero, WTF??? Keep calm, it's heavily necessary
    "A", "B", "C", "D", "E", "F",
    "G", "H", "I", "J", "K", "L",
    "M", "N", "O", "P", "Q", "R",
    "S", "T", "U", "V", "W", "X",
    "Y","Z"
  );
  vReturn <- vector(); # return vector, still empty
  for (n in vn) { # if you pass a vector with numbers
    if (
      ! is.numeric(n) ||
      is.na(n) ||
      is.nan(n) ||
      is.infinite(n) ||
      n <= 0
    ){ # code if thrash is passed
      vReturn <- c(vReturn, "");
    } else { # if all is fine
      iNumber <- n;
      sChar <- ""; # initiate the charnumber with empty string
      repeat {
        #if (iNumber %% length(vsAlphabet) == 0) iNumber <- iNumber + 1; # Skip the zero in the 27s arithmetic
        #print(iNumber / (length(vsAlphabet)))
        #print(floor(iNumber / (length(vsAlphabet))))
        iNumber <- iNumber + floor(iNumber / (length(vsAlphabet)));
        iLastChar <- iNumber %% length(vsAlphabet); # rightest chardigit
        iNumber <- floor(iNumber / length(vsAlphabet)); # chardigits left
        sChar <- paste(vsAlphabet[iLastChar+1], sChar, sep = ""); # fuse digits to charnumber
        if (iNumber <= 0) break(); # Break the loop, if no value left
      }
      vReturn <- c(vReturn, sChar); # add a charnumber to the returnvector
    }
  }
  return(vReturn);
}