---
title: "Design-Amateur"
date: "21. Juli 2019"
author: 'Marco Grimmeißen'
output:
  pdf_document:
    toc: yes
    number_sections: true
    keep_md: yes
    keep_tex: yes
    latex_engine: pdflatex
bibliography: bibliography.bib
fontsize: 11pt
params:
  version: "0.0.0"
  license: "LGPLv2"
---

<!---
 Load the Project to ceate the Examples
-->

\newpage
# Abstract
Version: 0.0.0

A R-Project which is able to create and solve experimental Designs, especially I- and D-Optimal Designs.

<!--[@Kronsbein2019]-->

# Theory
## Principle of Design of Experiment, explained by a full factorial Design

Design of Experiment is a method to determine if factors (parameters of an experiment you can set, e.g. temperature, pressure, ...) of an experiment have a effect to the response of the experiment and if they have, it is possible to estimate the magnitude of the effect.


| Number| temperature (A) | concentration (B) | catalyst (C) | yield (y) |
| ----: | :-------------: | :---------------: | :----------: | :-------: |
|     1 |               - |                 - |            - |           |
|     2 |               + |                 - |            - |           |
|     3 |               - |                 + |            - |           |
|     4 |               + |                 + |            - |           |
|     5 |               - |                 - |            + |           |
|     6 |               + |                 - |            + |           |
|     7 |               - |                 + |            + |           |
|     8 |               + |                 + |            + |           |

Table: A \(2^3\)-Full Factorial Design [@Flottmann2019, Page 16]

In the Table above you can see an example of a Full Factorial Design. It is created by a simple rule you recognise in the table.
In the next step you do the measurement. The result is the table below.


| Number| temperature (A) | concentration (B) | catalyst (C) | yield (y) |
| ----: | :-------------: | :---------------: | :----------: | :-------: |
|     1 |               - |                 - |            - |        59 |
|     2 |               + |                 - |            - |        74 |
|     3 |               - |                 + |            - |        50 |
|     4 |               + |                 + |            - |        69 |
|     5 |               - |                 - |            + |        50 |
|     6 |               + |                 - |            + |        81 |
|     7 |               - |                 + |            + |        46 |
|     8 |               + |                 + |            + |        79 |

Table: A \(2^3\)-Full Factorial Design after measurement [@Flottmann2019, Page 16]

To estimate the effects, you can do a regession with to a modell. A modell only for the main effects is for example:

\begin{equation}
  \hat{Y} = \beta_0 + \beta_1\cdot x_1 + \beta_2\cdot x_2 + \beta_3\cdot x_3
\end{equation}


```
##   temperature concentration catalyst
## 1          -1            -1       -1
## 2           1            -1       -1
## 3          -1             1       -1
## 4           1             1       -1
## 5          -1            -1        1
## 6           1            -1        1
## 7          -1             1        1
## 8           1             1        1
```

With the modell and the designmatrix D you can create the modell matrix M


```
##   mean temperature concentration catalyst
## 1    1          -1            -1       -1
## 2    1           1            -1       -1
## 3    1          -1             1       -1
## 4    1           1             1       -1
## 5    1          -1            -1        1
## 6    1           1            -1        1
## 7    1          -1             1        1
## 8    1           1             1        1
```

Now you can estimate the effects by regression with
\begin{equation}
  b = (X^TX)^{-1}X^T\cdot y
\end{equation}

It results in a vector with the \(\beta\)-coefficients, aka effects.


```
##                [,1]
## mean          64.25
## temperature   11.50
## concentration -2.50
## catalyst       0.75
```

## Speciality of Optimal Design


Optimal Designs are used, if you have a limited ammount of experiments and want to create a good design with a maximum of orhogonality whith all possible experiments.

In detail a a iterative optimization process is used. In Example you have a total ammount of seven experiments. You want to gather a maximum of information by keeping a maximum of orthogonality. A D-Optimal Design is quite a good choice for this case.

First the algorithm creates a "design matrix" with random numbers between the minimum and the maximum factor level


```
##             A          B          C
## 1  0.01495641 -0.4553899 -0.2839000
## 2 -0.38646299  0.2316586 -0.1423812
## 3 -0.14618467 -0.1406569 -0.8961934
## 4  0.38620416  0.3033113 -0.4716447
## 5 -0.82972806  0.1354755 -0.2024185
## 6 -0.54912677 -0.7729820  0.6722683
## 7 -0.45093895  0.1918506  0.7294425
```

In the next step for each level one matrix is created, the first value gets variated


```
##            A          B          C
## 1 -1.0000000 -0.4553899 -0.2839000
## 2 -0.3864630  0.2316586 -0.1423812
## 3 -0.1461847 -0.1406569 -0.8961934
## 4  0.3862042  0.3033113 -0.4716447
## 5 -0.8297281  0.1354755 -0.2024185
## 6 -0.5491268 -0.7729820  0.6722683
## 7 -0.4509390  0.1918506  0.7294425
```

and


```
##            A          B          C
## 1  1.0000000 -0.4553899 -0.2839000
## 2 -0.3864630  0.2316586 -0.1423812
## 3 -0.1461847 -0.1406569 -0.8961934
## 4  0.3862042  0.3033113 -0.4716447
## 5 -0.8297281  0.1354755 -0.2024185
## 6 -0.5491268 -0.7729820  0.6722683
## 7 -0.4509390  0.1918506  0.7294425
```

The goal of the Process is finding the most orhogonal design. The metric is the determinant \(|X^TX|\) that should be maximum, the matrix is is the model matrix. Example:


```
##   mean           A          B          C
## 1    1  0.01495641 -0.4553899 -0.2839000
## 2    1 -0.38646299  0.2316586 -0.1423812
## 3    1 -0.14618467 -0.1406569 -0.8961934
## 4    1  0.38620416  0.3033113 -0.4716447
## 5    1 -0.82972806  0.1354755 -0.2024185
## 6    1 -0.54912677 -0.7729820  0.6722683
## 7    1 -0.45093895  0.1918506  0.7294425
```

This process get repeated for every value


```
##            A          B          C
## 1  1.0000000 -0.4553899 -0.2839000
## 2 -0.3864630  0.2316586 -0.1423812
## 3 -0.1461847 -0.1406569 -0.8961934
## 4  0.3862042  0.3033113 -0.4716447
## 5 -0.8297281  0.1354755 -0.2024185
## 6 -0.5491268 -0.7729820  0.6722683
## 7 -0.4509390  0.1918506  0.7294425
```


```
##            A          B          C
## 1  1.0000000 -0.4553899 -0.2839000
## 2  1.0000000  0.2316586 -0.1423812
## 3 -0.1461847 -0.1406569 -0.8961934
## 4  0.3862042  0.3033113 -0.4716447
## 5 -0.8297281  0.1354755 -0.2024185
## 6 -0.5491268 -0.7729820  0.6722683
## 7 -0.4509390  0.1918506  0.7294425
```


```
##            A          B          C
## 1  1.0000000 -0.4553899 -0.2839000
## 2  1.0000000  0.2316586 -0.1423812
## 3 -1.0000000 -0.1406569 -0.8961934
## 4  0.3862042  0.3033113 -0.4716447
## 5 -0.8297281  0.1354755 -0.2024185
## 6 -0.5491268 -0.7729820  0.6722683
## 7 -0.4509390  0.1918506  0.7294425
```
...

```
##    A  B  C
## 1  1 -1  1
## 2  1  1 -1
## 3 -1 -1 -1
## 4  1  1  1
## 5 -1  1 -1
## 6 -1 -1  1
## 7 -1  1  1
```

It is possible, that the result isn't the best. To get the best result, the process could be repeated, until the result reaches convergence.
The following Matrix is the result of a D-Optimisation of the matrix before.

```
##    A  B  C
## 1  1 -1  1
## 2  1 -1 -1
## 3 -1 -1 -1
## 4  1  1 -1
## 5 -1  1 -1
## 6 -1 -1  1
## 7 -1  1  1
```

If you want to know more, read [@Goos2011]

# How to Use

After this short overview it's time to show you how to use the program.

The first step is loading the project in your workspace by running

```r
source("./main.R");
```

The Project contains three functions to work with Optimal Designs

## createOptDesign()
### Description
This function is for creating a design object
### Usage

```r
createOptDesign(
  runs = 12,
  defDesign = list(
    main = list(
      A = 1,
      B = 2,
      C = 3,
      D = 4
    ),
    multi = list(
      use = FALSE,   # Default: off
      c(1, 2),       # A × B
      c(1, 3),       # A × C
      c(1, 2, 3)     # A × B × C
    )
  ),
  factorLevels = c(-1, 1),
  orderMod = 1, # Ordnung des Modells
  criterion = "D",
  exchageAlg = "k",
  tXXDiag = FALSE
)
```
### Arguments
| Argument | Description |
| :-- | :-- |
| runs = 12 | Number of experiments |
| defDesign | Definition of the Design, it contains subarguments |
| defDesign = list(main) | Definition of effects contains name and number to define interactions |
| defDesign = list(multi) | Define Interactions. For example c(1, 2) defines A × B |
| defDesign = list(multi = list(use)) | Ignore Interactions, off by default |
| factorLevels = c(-1, 1) | Define the fevels of the factors, by default -1 and 1 |
| orderMod = 1 | Order of the used Model, by default linear |
| criterion = "D" | Optimality criterion. D and I are possible |
| exchageAlg  = "k" | Only k exchange algorithm is implemented yet. |
| tXXDiag = FALSE | To force complete diagonylity. Dangerous parameter, because optimal orthogonality isn't possible for all settings. Can result in an infinite loop |

### Example
Running the function with default arguments result in following list

```r
createOptDesign();
```

```
## $alert
## [1] 0
## 
## $log
## [1] "D-Optimal Design wurde erstellt"
## 
## $stat
## [1] TRUE
## 
## $numRuns
## [1] 12
## 
## $D
##        A  B  C  D
##  [1,] -1  1 -1  1
##  [2,]  1 -1 -1  1
##  [3,]  1  1 -1  1
##  [4,] -1  1 -1 -1
##  [5,]  1 -1  1 -1
##  [6,]  1  1 -1 -1
##  [7,] -1 -1  1  1
##  [8,] -1 -1 -1  1
##  [9,]  1  1  1  1
## [10,]  1 -1 -1 -1
## [11,] -1  1  1 -1
## [12,] -1 -1 -1 -1
## 
## $X
##       mean  A  B  C  D
##  [1,]    1 -1  1 -1  1
##  [2,]    1  1 -1 -1  1
##  [3,]    1  1  1 -1  1
##  [4,]    1 -1  1 -1 -1
##  [5,]    1  1 -1  1 -1
##  [6,]    1  1  1 -1 -1
##  [7,]    1 -1 -1  1  1
##  [8,]    1 -1 -1 -1  1
##  [9,]    1  1  1  1  1
## [10,]    1  1 -1 -1 -1
## [11,]    1 -1  1  1 -1
## [12,]    1 -1 -1 -1 -1
## 
## $M
## [1] FALSE
## 
## $Y
## [1] NA
## 
## $tXX
##      mean  A  B  C  D
## mean   12  0  0 -4  0
## A       0 12  0  0  0
## B       0  0 12  0  0
## C      -4  0  0 12  0
## D       0  0  0  0 12
## 
## $Var
##            [,1]
## mean 0.09375000
## A    0.08333333
## B    0.08333333
## C    0.09375000
## D    0.08333333
## 
## $VIF
##       [,1]
## mean 1.125
## A    1.000
## B    1.000
## C    1.125
## D    1.000
## 
## $factorLevels
## [1] -1  1
## 
## $errVariance
## [1] 1
## 
## $R
##      [,1]
## [1,]   NA
## 
## $R2
## [1] NA
## 
## $b
## [1] NA
## 
## $a
## [1] NA
## 
## $tLim
## [1] NA
## 
## $tVal
##      [,1]
## [1,]   NA
## 
## $tSig
##      [,1]
## [1,]   NA
## 
## $pLim
## [1] NA
```


## solveDesign()
### Description
Estimates (obviously) the effects

### Usage

```r
solveDesign(design, Y, a = 0.05, ns = 3)
```

### Arguments
| Argument | Description |
| :-- | :-- |
| design | Design list from e.g. createOptDesign() |
| a = 0.05 | \(\alpha\)-level |
| ns = 3 | n times \(\sigma\) criterion DEPRECATED, removed soon |

### Example

```r
design <- createOptDesign()
solveDesign(design = design, c(1,1,1,1,1,1,1,1,1,1,1,2))
```

```
## $alert
## [1] 0
## 
## $log
## [1] "Berechnung fertig"
## 
## $stat
## [1] TRUE
## 
## $numRuns
## [1] 12
## 
## $D
##        A  B  C  D
##  [1,]  1 -1  1  1
##  [2,] -1 -1  1  1
##  [3,]  1  1  1 -1
##  [4,]  1  1 -1 -1
##  [5,] -1  1  1 -1
##  [6,] -1 -1 -1 -1
##  [7,] -1  1 -1  1
##  [8,] -1 -1 -1 -1
##  [9,]  1 -1 -1  1
## [10,]  1  1 -1  1
## [11,] -1  1 -1  1
## [12,] -1  1  1  1
## 
## $X
##       mean  A  B  C  D
##  [1,]    1  1 -1  1  1
##  [2,]    1 -1 -1  1  1
##  [3,]    1  1  1  1 -1
##  [4,]    1  1  1 -1 -1
##  [5,]    1 -1  1  1 -1
##  [6,]    1 -1 -1 -1 -1
##  [7,]    1 -1  1 -1  1
##  [8,]    1 -1 -1 -1 -1
##  [9,]    1  1 -1 -1  1
## [10,]    1  1  1 -1  1
## [11,]    1 -1  1 -1  1
## [12,]    1 -1  1  1  1
## 
## $M
## [1] FALSE
## 
## $Y
##       [,1]
##  [1,]    1
##  [2,]    1
##  [3,]    1
##  [4,]    1
##  [5,]    1
##  [6,]    1
##  [7,]    1
##  [8,]    1
##  [9,]    1
## [10,]    1
## [11,]    1
## [12,]    2
## 
## $tXX
##      mean  A  B  C  D
## mean   12 -2  2 -2  2
## A      -2 12  0  0  0
## B       2  0 12  0  0
## C      -2  0  0 12  0
## D       2  0  0  0 12
## 
## $Var
##             [,1]
## mean 0.008370536
## A    0.007672991
## B    0.007672991
## C    0.007672991
## D    0.007672991
## 
## $VIF
##            [,1]
## mean 0.10044643
## A    0.09207589
## B    0.09207589
## C    0.09207589
## D    0.09207589
## 
## $factorLevels
## [1] -1  1
## 
## $errVariance
## [1] 0.08928571
## 
## $R
##              [,1]
##  [1,] -0.08333333
##  [2,] -0.22916667
##  [3,] -0.08333333
##  [4,]  0.10416667
##  [5,] -0.22916667
##  [6,]  0.10416667
##  [7,] -0.18750000
##  [8,]  0.10416667
##  [9,]  0.10416667
## [10,] -0.04166667
## [11,] -0.18750000
## [12,]  0.62500000
## 
## $R2
## [1] 0.625
## 
## $b
##             [,1]
## mean  1.06250000
## A    -0.07291667
## B     0.07291667
## C     0.09375000
## D     0.07291667
## 
## $a
## [1] 0.05
## 
## $tLim
## [1] 1.795885
## 
## $tVal
##            [,1]
## mean 11.6132109
## A    -0.8324237
## B     0.8324237
## C     1.0702591
## D     0.8324237
## 
## $tSig
##       [,1]
## mean  TRUE
## A    FALSE
## B    FALSE
## C    FALSE
## D    FALSE
## 
## $pLim
##              [,1]
## mean 8.139437e-08
## A    2.114371e-01
## B    2.114371e-01
## C    1.537124e-01
## D    2.114371e-01
```


## updateDesign()
### Description
Funktion for checking a design list for errors and correct them, or update the values after modifying the design

### Arguments
| Argument | Description |
| :-- | :-- |
| design | Design list from e.g. createOptDesign() |

## Design list
### Description
The data format of the Project. It is a list containing informaton of a design

### Entries
| Entry | Description |
| :-- | :-- |
| design | Design list from e.g. createOptDesign() |
| alert | Status. If it is zero, the process was successful |
| log | A text info describing the last operation |
| numRuns | Number of experiments |
| D | Design matrix |
| X | Model matrix |
| M | Moment Matrix for I-Optimality criterion. If D-Optimality is choosen, the value is FALSE
| Y | Response vector |
| tXX | \(Y^TX\)-matrix |
| Var | Variance |
| VIF | Variance inflation factors |
| factorLevels | Factor levels (obviously) |
| errVariance | Error variance \(s^2\) |
| R | Error of regression |
| R2 | Sum of Squares of R |
| b | Regression coefficients / Half of effects|
| a | \(\alpha\) |
| tLim | Significance limit for t-Values DEPRECATED |
| tVal | t-Values for effects |
| tSig | Is t-Value significant? DEPRECATED |
| pLim | p-Value |

\newpage
# References

