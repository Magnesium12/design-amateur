solveDesign <- function(design, Y, a = 0.05, ns = 3) { #Y as list!!!
  varOfY <- function(Y = Y) { 
    vVar <- apply(Y, 1, var);
    if (match(NA, vVar)) vVar[] <- 0;
    return(vVar);
  }
  # Falls mehrere Werte für Y
  
  mY <- as.matrix(Y);
  mY[is.na(mY)] <- 0;
  dfY <- data.frame(
    mean = apply(mY, 1, mean),
    var = varOfY(Y = mY)
  );
  
  # Lösen, Regression
  vb <- solve(t(design$X)%*%design$X)%*%t(design$X)%*%as.matrix(dfY$mean);
  
  # Feher der Regression
  vR <- dfY$mean - design$X%*%vb; # nachbessern
  rss <- as.numeric(t(vR)%*%vR);

  # Error Variance
  #rErrVariance <- ( 1/(design$numRuns - ncol(design$X)) ) * rss;
  rErrVariance <- ( 1/(design$numRuns - length(vb)) ) * rss; #*ncol(mY)
  
 
  #Standard Error
  #rStandardError <- 2*sqrt(rErrVariance)/sqrt(design$numRuns*ncol(mY));
  rStandardError <- sqrt(rErrVariance);
  
  # ANOVA -----
  # Table with SSb's
  #Calc of SSw's
  #llANOVA <- list();
  #for (iEff in 1:ncol(design$X)) {
  #  llANOVA[[iEff]] <- doe_anova(effectColumn = as.vector(design$X[,iEff]), mY = mY, a = a);
  #}
  #names(llANOVA) <- colnames(design$X);
  #print(llANOVA[1]); # DEBUG
  
  #t- Werte
  vVar <- rErrVariance * design$Var; # stimmt das????????

  dfTtest <- data.frame(
    b = vb,
    var = vVar
  );
  #
  
  vt <- apply(dfTtest, 1, function(r) {return(r[[1]]/sqrt(r[[2]]));});
  
  rt_grenze <- qt(1-a, df = design$numRuns - 1); #*ncol(mY) ###
  vtbool <- sapply(vt, function(t, gr = rt_grenze) {if (abs(t)>gr) {return(TRUE);} else {return(FALSE);}});
  
  #n*s Kriterium
  #vsbool <- sapply(dfTtest$b, function(b, s = rStandardError) {if (abs(b)>(ns*s)) {return(TRUE);} else {return(FALSE);}});

  #P Wert
  vp <- pt(-abs(vt), df = design$numRuns - 1); # Prüfen *ncol(mY) ###
  return(createOutputDesign(
    alert = 0,
    log = c(
      "Calculation has finished",
      "Berechnung fertig"
    )[CONF$lang],
    stat = TRUE,
    numRuns = design$numRuns,
    D = design$D,
    X = design$X,
    M = design$M,
    Y = mY,
    tXX = design$tXX,
    Var = vVar,
    VIF = rErrVariance * design$VIF , # Prüfen
    factorLevels = design$factorLevels,
    errVariance = rErrVariance,
    R = vR,
    R2 = rss,
    b = vb,
    a = a,
    tLim = rt_grenze,
    tVal = as.matrix(vt),
    tSig = as.matrix(vtbool),
    #numSigma = ns,
    #Sig3S = as.matrix(vsbool),
    pLim = as.matrix(vp)
  ));
}

updateDesign <- function(design) {
  
}
