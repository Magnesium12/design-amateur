
doe_anova <- function(effectColumn, mY, a = 0.05) {
  # Mean -----
  rMeanY <- mean(mY);
  # Dataframe for seperation -----
  dfANOVA <- data.frame(level = effectColumn); #design$X[,iEff]);
  dfANOVA <- cbind(dfANOVA, mY);
  
  
  
  # Levels / degrees of Freedom
  vLevels <- unique(sort(dfANOVA$level));
  # Prepare Lists/Vectors
  ldfANOVA <- list();
  vrMeanGroup <- vector(mode = "numeric", length = length(vLevels));
  vSSw <- vector(mode = "numeric", length = length(vLevels));
  vNumOfLevels <- vector(mode = "numeric", length = length(vLevels));
  
  # Is Design symetric -----
  for (ii in 1:length(vLevels)) {
    vNumOfLevels <- length(effectColumn[effectColumn == vLevels[ii]]);
  }
  if (length(unique(sort(vNumOfLevels))) != 1) return(FALSE);
  
  # degrees of freedom
  iK <- vNumOfLevels[1];
  
  iG <- length(vLevels);
  
  
  # Sort Groups -----
  for (ii in 1:length(vLevels)) ldfANOVA[[ii]] <- dfANOVA[dfANOVA$level == vLevels[ii],];
  
  # Means of Groups -----
  for (ii in 1:length(vrMeanGroup)) vrMeanGroup[ii] <- mean(ldfANOVA[[ii]][,-1]);
  
  # SSt -----
  SSt <- sum((mY - rMeanY)^2);
  MSt <- SSt / (iG * iK - 1);
  
  # SSb -----
  SSb <- sum((vrMeanGroup - rMeanY)^2);
  MSb <- SSb / (iG - 1)
  
  # SSw -----
  for (ii in 1:length(vSSw)) { #xx
    vSSw[ii] <- sum((ldfANOVA[[ii]][,-1] - vrMeanGroup[ii])^2);
  }
  SSw <- sum(vSSw);
  MSw <- SSw / (iG * (iK - 1))
  
  # return -----
  return(list(
    G = iG,
    K = iK,
    SSt = SSt,
    MSt = MSt,
    SSb = SSb,
    MSb = MSb,
    SSw = SSw,
    MSw = MSw,
    etaSq = SSb/SSt,
    Femp = MSb/MSw,
    Ftab = qf(1-a, df1 = iG, df2 = iK)
  ));
}