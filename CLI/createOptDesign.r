createOptDesign <- function( # Exchange Algorithm
  runs = 12,
  defDesign = list(
    main = list(
      A = 1,
      B = 2,
      C = 3,
      D = 4
    ),
    multi = list(
      use = FALSE,   #c(2, 3): zweifach und dreifach
      c(1, 2),
      c(1, 3),
      c(1, 2, 3)
    )
  ),
  factorLevels = c(-1, 1),
  orderMod = 1, # Ordnung des Modells
  criterion = "D",
  exchageAlg = "k",
  tXXDiag = FALSE
){
  if (
      # Numerische Eingabe?
      (! is.numeric(runs)) || ( is.nan(runs)) ||
      # Kriterium
      (! any(criterion == c("D", "I"))) ||
      # Algorithmus: Coordinate Exchange(k) oder Point Exchange (p)[nicht implementiert] 
      (! any(exchageAlg == c("k", "p"))) 
    ) {
    return(list(
      alert = 1,
      log = c(
        "Invalid input",
        "Ungültige Eingabe"
      )[CONF$lang],
      stat = FALSE
    ));
  }
  
  # Erzeugen von mD:
  mD <- matrix(data = NA, ncol = 1, nrow = runs);
  for (ii in 1:length(defDesign)) {
    switch (names(defDesign[ii]),
      main = { # Haupeffekte
        dfMainEff <- getMainEff(defDesignMainObj = defDesign[[ii]]) # in [lib/helper.r]
        for (ij in 1:nrow(dfMainEff)) { # Erzeugung der Spalten der Designmatrix
          mtmp <- matrix(data = NA, nrow = runs, ncol = 1);
          colnames(mtmp) <- as.character(dfMainEff$name[ij]);
          mD <- cbind(mD, mtmp);
        }
      },
      multi = {}
    );
  }
  rm(mtmp, dfMainEff); # Löschung von Hilfsvariablen
  mD <- mD[, -1]; # Löschen der ersten Spalte (Hilfsspalte)
  #mX <- calcMX(mD, defDesign = defDesign);
  
  #Wahl des Kriteriums
  switch (exchageAlg,
    k = { # k-Exchange Algorithmus
      switch (criterion,
        I = {
          lMfull <- calcMM(defDesign = defDesign, factorLevels = factorLevels); # Hängt vom Modell ab
          #mM <- (2^orderMod) * lMfull$M; # Hängt vom Modell ab
          mM <- lMfull$M; # Hängt vom Modell ab
        },
        D = {mM <- FALSE;},# Bei I-Optimal notwendig
        {stop("ERROR");}
      );
      
      repeat { # Stellt komplette Diagonalität fest (optional)
        repeat { # Erzeugt mD mit Determinante, bei Bedarf
          mD[1:length(mD)] <- runif(length(mD), min = min(factorLevels), max = max(factorLevels));
          mX <- calcMX(mD = mD, defDesign = defDesign);
          if ((criterion == "I") || (is.numeric(try(det(t(mX)%*%mX), silent = TRUE)))) break(1); # Bei I nur ein Durchgang
        } # Erzeugt mD mit Determinante
        
        lmDLevel <- list(); # Anlegen Liste mD
        lmXLevel <- list(); # Anlegen Liste mX
        vOptCrit <- vector(mode = "numeric", length = length(factorLevels)); # Vektor mit Det mXX / Liste Average Relative Variance of Prediction

        repeat { #Cyklische Optimierung
          mDold <- mD;
          
          #writeLines("Schritt 0:\n\nD")
          #print(mD); writeLines("\n\nX");
          #mTmpX <- calcMX(mD = mD, defDesign = defDesign)
          #print(mTmpX); writeLines("\n\nX^t*X")
          #print(t(mTmpX)%*%mTmpX); writeLines("\n\n");
          #rm(mTmpX);
          
          for (ii in 1:length(mD)) {
            for (iLevel in 1:length(factorLevels)) { # Bsp -1, 1 in factorLevels, mX berechnen
              # Abktuelles mD einsetzen
              lmDLevel[[iLevel]] <- mD; 
              # Optimierungspossition durch Level ersetzen
              lmDLevel[[iLevel]][ii] <- factorLevels[iLevel]; 
              # Berechnen von mX für die Verschidenen Level
              lmXLevel[[iLevel]] <- calcMX(mD = lmDLevel[[iLevel]], defDesign = defDesign);
              
              # Optimierungskriterium
              switch (criterion,
                D = {
                  # Berechnung der Det
                  vOptCrit[iLevel] <- det(t(lmXLevel[[iLevel]])%*%lmXLevel[[iLevel]]);
                },
                I = {
                  # Average relative Variance of Prediction
                  vOptCrit[[iLevel]] <- (2^(-orderMod)) * sum(diag(
                    solve(t(lmXLevel[[iLevel]]) %*% lmXLevel[[iLevel]]) %*% mM
                  )); # Spur
                },
                {stop("ERROR");}
              );
            }
            # Optimierung
            switch (criterion,
              D = {iOpt <- order(vOptCrit, decreasing = TRUE)[1];}, # Levelindex mit größter Det
              I = {iOpt <- order(vOptCrit, decreasing = FALSE)[1];}, # Levelindex mit kleinstem Wert
              {stop("ERROR");}
            );
            mD <- lmDLevel[[iOpt]];
            #writeLines(paste("Schritt " , ii, ":\n\nD", sep = ""));
            #print(mD); writeLines("\n\nX");
            #print(lmXLevel[[iOpt]]); writeLines("\n\nX^t*X");
            #print(t(lmXLevel[[iOpt]])%*%lmXLevel[[iOpt]]); writeLines("\n\n");
          }
          if (all(mD == mDold)) {
            break(1); # Läuft bis Konstanz
          } # Abbruchbedingung der Det Optimierung
        } # Ende repeat Cyklische Optimierung
        mX <- calcMX(mD = mD, defDesign = defDesign);
        tXX <- t(mX)%*%mX;
        if (isFALSE(tXXDiag) || is.diagonal.matrix(tXX)) {
          rm(lmDLevel, lmXLevel, iOpt, vOptCrit, mDold); # Aufräumen 
          break(1); # Abbruchbedingung repeat
        } # Abbruchbedingung für komplette Diagonalität
      } # Ende repeat komplette Diagonalität
      vVar <- as.matrix(diag(solve(tXX))); # in solveDesign
      vVIF <- vVar*runs; # in solveDesign
      switch (criterion,
        D = {sLog <- c(
          "D-Optimal design was created",
          "D-Optimal Design wurde erstellt"
        )[CONF$lang];},
        I = {sLog <- c(
          "I-Optimal design was created",
          "I-Optimal Design wurde erstellt"
        )[CONF$lang];},
        {stop("ERROR");}
      );
      return(createOutputDesign(
        alert = 0,
        log = sLog,
        stat = TRUE,
        numRuns = runs,
        D = mD,
        X = mX,
        M = mM,
        tXX = tXX,
        Var = vVar,
        VIF = vVIF,
        factorLevels = factorLevels,
        errVariance = 1
      ));
    },
    c = {}
  )
}

#print(createOptDesign());

