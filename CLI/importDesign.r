importDesign <- function(
  D = matrix(data = c(1, -1, -1, 1), ncol = 2, nrow = 2),
  multi = list(
    use = FALSE,   #c(2, 3): zweifach und dreifach
    c(1, 2),
    c(1, 3),
    c(1, 2, 3)
  )
){
  mD <- apply(D, c(1, 2), as.numeric);
  rRuns <- nrow(mD);
  mean <- vector(mode = "numeric", length = rRuns);
  mean[] <- 1;
  #mX <- cbind(mean, mD);
  
  mX <- calcMX(mD = mD, defDesign = list(main = NA, multi = multi));
  tXX <- t(mX) %*% mX;
  
  vVar <- try(as.matrix(diag(solve(tXX))), TRUE);
  if (!(is.numeric(vVar))) vVar <- NA;
  vVIF <- vVar * rRuns;
  
  vFactorLevels <- unique(sort(mD));
  return(createOutputDesign(
    alert = 0,
    log = "",
    stat = TRUE,
    numRuns = rRuns,
    D = mD,
    X = mX,
    M = NA,
    tXX = tXX,
    Var = vVar,
    VIF = vVIF,
    factorLevels = vFactorLevels,
    errVariance = 1
  ));
}