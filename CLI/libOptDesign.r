#Extrahiert die Haupeffekte aus der Designvorgabe und sortiert diese
getMainEff <- function(defDesignMainObj) {
  pos <- vector(); # Vektor mit Possitionen
  name <- vector(); # Vektor mit Bezeichnung
  for (ij in 1:length(defDesignMainObj)) { # Alle Hauptelemente werden durchlaufen
    pos <- c(pos, defDesignMainObj[[ij]]);
    name <- c(name, names(defDesignMainObj[ij]));
  }
  dfMainEff <- data.frame( # Erzeugung eines Datframes
    pos = pos,
    name = name
  )
  dfMainEff <- dfMainEff[order(dfMainEff$pos),]; # Sortierung nach Possition
  return(dfMainEff);
}

createOutputDesign <- function(
  alert = NA,
  log = c(
    "Not defined action",
    "Umbestimmte Aktion"
  )[CONF$lang],
  stat = FALSE,
  numRuns = NA,
  D = matrix(data = c(NA), ncol = 1, nrow = 1),
  X = matrix(data = c(NA), ncol = 2, nrow = 1),
  M = matrix(data = c(NA), ncol = 1, nrow = 1),
  Y = NA,
  tXX = matrix(data = c(NA), ncol = 2, nrow = 2),
  Var = NA,
  VIF = matrix(data = c(NA), ncol = 1, nrow = 1), # Prüfen
  factorLevels = c(0),
  errVariance = NA,
  R = matrix(data = c(NA), ncol = 1, nrow = 1),
  R2 = NA,
  b = c(NA),
  a = NA,
  tLim = NA,
  tVal = matrix(data = c(NA), ncol = 1, nrow = 1),
  tSig = matrix(data = c(NA), ncol = 1, nrow = 1),
  #numSigma = NA,
  #Sig3S = NA,
  pLim = NA
) {
  return(list(
    alert = alert,
    log = log,
    stat = stat,
    numRuns = numRuns,
    D = D,
    X = X,
    M = M,
    Y = Y,
    tXX = tXX,
    Var = Var,
    VIF = VIF , # Prüfen
    factorLevels = factorLevels,
    errVariance = errVariance,
    R = R,
    R2 = R2,
    b = b,
    a = a,
    tLim = tLim,
    tVal = tVal,
    tSig = tSig,
    #numSigma = numSigma,
    #Sig3S = Sig3S,
    pLim = pLim
  ));
}

genListTree <- function(ncol = 2, nrow = 2) {
  # Erzeugen einer "leeren" Matrix
  lPre <- vector(mode = "list", length = ncol);
  llTree <- lapply(vector(mode = "list", length = nrow), function(u) {return(lPre)});
  rm(lPre);
  return(llTree);
}

calcMM <- function(defDesign, factorLevels) {
  genItem <- function(varName, factor = 1, exponent = 1) { # Einen Ausdruck erzeugen
    if (is.numeric(factor) && is.numeric(exponent) ) {
      return(list(
        type = "Item",
        var = varName,
        factor = factor,
        exponent = exponent
      ));
    } else {
      return(FALSE);
    }
  }
  
  checkItem <- function(itemObj) { # Ist Objekt ein Item
    # Noch Offen
    if (any(itemObj$type == c("Expr", "Item"))) {
      return(list(
        alert = TRUE,
        type = itemObj$type
      ))
    } else {
      return(list(
        alert = FALSE,
        type = ""
      ));
    };
  }
  
  genExpr <- function(exprI, method = "product") {
    mergeProducts <- function(lExprI) {
      lProd <- list();
      
      for (obj in lExprI) {
        #print(obj)
        switch (obj$type,
                Expr = {
                  if (! (obj$method == "product")) next(1);
                  lProd <- append(lProd, mergeProducts(lExprI = obj$item));
                },
                Item = {
                  lProd <- append(lProd, list(obj)); # list(), um obj als ein Listenelement zusammenzufassen
                }
        )
      }
      return(lProd);
    } # Fasst verschachtelte Produkte zusammen, rekursiv
    if (!(
      any((method == c("product", "summ", "quotient", "difference"))) &&
      all(sapply(exprI, function(u) {return(checkItem(u)$alert);})) # Überprüft alle Elemente
    )) return(FALSE);
    #Rekursiver Ausdruck.
    cExprI <- mergeProducts(lExprI = exprI);
    #cExprI <- exprI;
    lRet <- list(
      type = "Expr",
      item = cExprI,
      method = method
    )
  }
  
  calcFunc <- function(defDesign) {
    fnk <- list(
      list(
        name = "mean", 
        item = genItem(varName = "m", factor = 1, exponent = 0))
    );
    for (ii in 1:length(defDesign)) {
      switch (names(defDesign[ii]),
              main = {
                dfMainEff <- getMainEff(defDesign[[ii]]);
                iNumMainEff <- nrow(dfMainEff); # Anzahl der Haupeffekte
                for (ii in 1:iNumMainEff) { # Erzeugung der Spalten der Designmatrix
                  fnk[[length(fnk)+1]] <- list(
                    name = as.character(dfMainEff$name[ii]),
                    item = genItem(varName = paste("x", ii, sep = ""), factor = 1, exponent = 1)
                  )
                }
              },
              multi = {
                if (any(names(defDesign[[ii]]) == "use")) {
                  if (is.numeric(defDesign[[ii]]$use)) {
                    #Zählen: Noch einfügen
                    #print("A")
                  } 
                } else {
                  for (ij in 1:length(defDesign[[ii]])) {
                    #Benutzerdefiniert zählen, nach expliziter Angabe
                    tmpname <- "";
                    vColNum <- defDesign[[ii]][[ij]];
                    lTmpItem <- list();
                    for (iColNum in defDesign[[ii]][[ij]]) { #Manuell angegebene Wechselwirkungen
                      tmpname <- paste(tmpname, " x ", fnk[[iColNum + 1]]$name, sep ="");
                      lTmpItem <- append(lTmpItem, list(fnk[[iColNum + 1]]$item));
                    }
                    #print(lTmpItem)
                    fnk[[length(fnk)+1]] <- list(
                      name = substring(tmpname, 4),
                      item = genExpr(lTmpItem, method = "product")
                    )
                    rm(lTmpItem);
                    #colnames(tmpcol) <- substring(tmpname, 4);
                    #mX <- cbind(mX, tmpcol);
                  }
                }
              },
              {}
      )
    }
    return(list(
      fnk = fnk,
      numMainEff = iNumMainEff
    ));
  }
  
  genMpre <- function(func) {
    #Erzeugen der Vorläufermatrix (Verschachtelte Liste, da matrix() ungeeignet)
    iDim <- length(func);
    llMpre <- genListTree(ncol = iDim, nrow = iDim);
    
    #Multiplizieren der Ausdrücke
    for (ii in 1:iDim) { #↓
      for (ij in 1:iDim) { #→
        if (ii <= ij) {
          llMpre[[ii]][[ij]] <- genExpr(
            list(
              func[[ii]]$item,
              func[[ij]]$item
            ),
            method = "product"
          );
        } else {
          llMpre[[ii]][[ij]] <- NULL;
        }
      }
    }
    # Aus Oberer Dreiecksmatrix komplette Matrix berechnen:
    #for (ii in 1:iDim) { #↓
    #  for (ij in 1:iDim) { #→
    #    if (ii < ij) {
    #      llMpre[[ij]][[ii]] <- llMpre[[ii]][[ij]];
    #    }
    #  }
    #}
    
    return(llMpre);
  }
  
  mergeEquMpre <- function(Mpre, numMainEff) {
    mergeEqu <- function(equ) {
      switch (equ$type,
              Expr = { # Objekt muss vom Typ Expr sein
                if (!(equ$method == "product")) return(FALSE); # Abbruch bei Falschem Expr
                #Leere Vektoren für df
                var <- vector(mode = "character");
                factor <- vector(mode = "numeric");
                exponent <- vector(mode = "numeric");
                # Vektoren werden mit den Eigenschaften der Faktoren befüllt
                for (obj in equ$item) {
                  #print("F1")
                  if (!(obj$type == "Item")) return(FALSE); # Fehler wenn ein nicht Item Objekt dabei ist
                  var <- c(var, as.character(obj$var));
                  factor <- c(factor, obj$factor);
                  exponent <- c(exponent, obj$exponent);
                }
                dfEqu <- data.frame( # df wird erzeugt
                  var = var, factor = factor, exponent = exponent
                );
                
                dfEqu <- dfEqu[order(dfEqu$var),]; #df wird nach Namen der Variablen sortiert
                #print(dfEqu)
                
                # Leere vektoren für zusammengefassten df, gleiche Namen wie oben
                var <- vector(mode = "character");
                factor <- vector(mode = "numeric");
                exponent <- vector(mode = "numeric");
                
                vVarNames <- as.vector(dfEqu[!duplicated(dfEqu$var),]$var); # Liste mit Namen der Variablen
                #print(vVarNames)
                for (varObj in vVarNames) { # Namen werden abgearbeitet
                  #print("F2")
                  dfTmp <- dfEqu[dfEqu$var == varObj,]; # Zeilen mit gleichem Namen werden in df gespeichert
                  var <- c(var, varObj); # Name der var
                  factor <- c(factor, prod(dfTmp$factor)); # Produkt der Faktoren
                  exponent <- c(exponent, sum(dfTmp$exponent)); # Summe der Exponenten
                }
                dfEquMerged <- data.frame(
                  var = var, factor = factor, exponent = exponent
                ); #neuer df
                rm(var, factor, exponent, dfTmp); # tmp var löschen
                #stop("X")
                
                #print(dfEquMerged)
                
                #stop()
                
                lItems <- list();
                for (ii in 1:nrow(dfEquMerged)) { # Liste mit Faktoren (Items) befüllen
                  #print("F3")
                  #print(dfEquMerged[ii,])
                  #print(as.character(dfEquMerged[ii,]$exponent))
                  lItems <- append(lItems,
                                   list(genItem(
                                     varName = as.character(dfEquMerged[ii,]$var),
                                     factor = as.numeric(dfEquMerged[ii,]$factor),
                                     exponent = as.numeric(dfEquMerged[ii,]$exponent)
                                   ))
                  );
                }
                rm(dfEquMerged);
                #stop()
                #print(lItems);
                return(
                  genExpr(exprI = lItems, method = "product") # Expr aus Liste erzeugen
                );
              },
              {
                # Fehler
                return(FALSE);
              }
      )
    }
    # Erzeugen einer "leeren" Matrix
    iDim <- length(Mpre);
    llMpreMerge <- genListTree(ncol = iDim, nrow = iDim);
    
    # Matrix Durcharbeiten
    for (ii in 1:iDim) { #↓
      for (ij in 1:iDim) { #→
        if (ii <= ij) {
          llMpreMerge[[ii]][[ij]] <- mergeEqu(Mpre[[ii]][[ij]]);
        }
      }
    }
    return(llMpreMerge);
  }
  
  integrateMpre <- function(MpreMerge, numMainEff, factorLevels) {
    integrateEqu <- function(equ, numMainEff = numMainEff, factorLevels = factorLevels) {
      
      switch (equ$type,
              Expr = {
                if (!(equ$method == "product")) return(FALSE);
                vFactors <- c();
                for (fac in equ$item) {
                  if (!(fac$type == "Item")) return(FALSE);
                  #if (any(fac$var == paste("x", 1:numMainEff, sep = "")))
                  varName <- fac$var;
                  sIntFunc <- paste("function(", varName, ") {return(",
                                    fac$factor,"*", varName, "^", fac$exponent,
                                    ");}", sep = "");
                  intFunc <- eval(parse(text = sIntFunc));
                  rIntegr <- integrate(intFunc, lower = min(factorLevels), upper = max(factorLevels));
                  vFactors <- c(vFactors, rIntegr$value); # /(max(factorLevels) - min(factorLevels)));
                  #print(sIntFunc); stop();
                }
                
                return(prod(vFactors));
              },
              {return(FALSE);}
      );
    }
    # Erzeugen einer "leeren" Matrix
    iDim <- length(Mpre);
    llM <- genListTree(ncol = iDim, nrow = iDim);
    mM <- matrix(data = NA, ncol = iDim, nrow = iDim);
    # Matrix Durcharbeiten
    for (ii in 1:iDim) { #↓
      for (ij in 1:iDim) { #→
        if (ii <= ij) {
          #llM[[ii]][[ij]] <- integrateEqu(MpreMerge[[ii]][[ij]],
          #  numMainEff = numMainEff, factorLevels = factorLevels
          #);
          mM[ii, ij] <- integrateEqu(MpreMerge[[ii]][[ij]],
                                     numMainEff = numMainEff, factorLevels = factorLevels
          );
          if (ii != ij) mM[ij, ii] <- mM[ii, ij];
        }
      }
    }
    return(mM);
  }
  
  # Funktionsobjekt erzeugen
  funcM <- calcFunc(defDesign = defDesign);
  #return(funcM);
  
  #Mpre Erzeugen
  Mpre <- genMpre(func = funcM$fnk);
  
  #Gleichungen zusammenfügen
  MpreMerge <- mergeEquMpre(Mpre = Mpre);
  
  #Mpre Integrieren → M
  mM <- integrateMpre(MpreMerge = MpreMerge, numMainEff = func$numMainEff, factorLevels = factorLevels);
  
  #Ausgabe der Ergebnisse
  return(list(
    fnk = funcM$fnk,
    Mpre = Mpre,
    MpreMerge = MpreMerge,
    M = mM
  ));
}

# -----
#calcMXold <- function(mD, twoFacWW) { # Nicht mehr in Verwendung
#  mX <- matrix(ncol = 1, nrow = nrow(mD), data = 1);
#  if (isTRUE(twoFacWW)) {
#    mX <- cbind(mX, mD);
#    for (ii in 1:(ncol(mD)-1)) {
#      for (ij in (ii+1):ncol(mD)) {
#        mX <- cbind(mX, mD[,ii]*mD[,ij]);
#        #print(paste(ii, ij))
#      }
#    }
#    return(mX);
#  } else {
#    return(cbind(mX, mD));
#  }
#}
# -----

calcMX <- function(mD, defDesign) { # Berechnet Modellmatrix aus Designmatrix
  mX <- matrix(ncol = 1, nrow = nrow(mD), data = 1);
  colnames(mX) <- "mean";
  mCol <- matrix(ncol = 1, nrow = nrow(mD), data = 1);
  for (ii in 1:length(defDesign)) {
    switch (names(defDesign[ii]),
            main = { # Haupteffekte, mD anfügen
              mX <- cbind(mX, mD);
            },
            multi = {
              if (any(names(defDesign[[ii]]) == "use")) {
                if (is.numeric(defDesign[[ii]]$use)) {
                  #Zählen: Noch einfügen
                  #print("A")
                } 
              } else {
                for (ij in 1:length(defDesign[[ii]])) {
                  #Benutzerdefiniert zählen, nach expliziter Angabe
                  tmpname <- "";
                  tmpcol <- mCol;
                  for (iColNum in defDesign[[ii]][[ij]]) { #Manuell angegebene Wechselwirkungen
                    tmpname <- paste(tmpname, " x ", colnames(mD)[iColNum], sep ="");
                    tmpcol <- tmpcol * as.matrix(mD[,iColNum]);
                  }
                  colnames(tmpcol) <- substring(tmpname, 4);
                  mX <- cbind(mX, tmpcol);
                }
              }
            }
    )
  }
  return(mX);
}
#calcMX <- calcMXb;

